using challenge.Controllers;
using challenge.Data;
using challenge.Models;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.TestHost;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using code_challenge.Tests.Integration.Extensions;

using System;
using System.IO;
using System.Net;
using System.Net.Http;
using code_challenge.Tests.Integration.Helpers;
using System.Text;
using System.Collections.Generic;

namespace code_challenge.Tests.Integration
{
    [TestClass]
    public class EmployeeControllerTests
    {
        private static HttpClient _httpClient;
        private static TestServer _testServer;

        [ClassInitialize]
        public static void InitializeClass(TestContext context)
        {
            _testServer = new TestServer(WebHost.CreateDefaultBuilder()
                .UseStartup<TestServerStartup>()
                .UseEnvironment("Development"));

            _httpClient = _testServer.CreateClient();
        }

        [ClassCleanup]
        public static void CleanUpTest()
        {
            _httpClient.Dispose();
            _testServer.Dispose();
        }

        [TestMethod]
        public void CreateEmployee_Returns_Created()
        {
            // Arrange
            var employee = new Employee()
            {
                Department = "Complaints",
                FirstName = "Debbie",
                LastName = "Downer",
                Position = "Receiver",
            };

            var requestContent = new JsonSerialization().ToJson(employee);

            // Execute
            var postRequestTask = _httpClient.PostAsync("api/employee",
               new StringContent(requestContent, Encoding.UTF8, "application/json"));
            var response = postRequestTask.Result;

            // Assert
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);

            var newEmployee = response.DeserializeContent<Employee>();
            Assert.IsNotNull(newEmployee.EmployeeId);
            Assert.AreEqual(employee.FirstName, newEmployee.FirstName);
            Assert.AreEqual(employee.LastName, newEmployee.LastName);
            Assert.AreEqual(employee.Department, newEmployee.Department);
            Assert.AreEqual(employee.Position, newEmployee.Position);
        }

        [TestMethod]
        public void GetEmployeeById_Returns_Ok()
        {
            // Arrange
            var employeeId = "16a596ae-edd3-4847-99fe-c4518e82c86f";
            var expectedFirstName = "John";
            var expectedLastName = "Lennon";

            // Execute
            var getRequestTask = _httpClient.GetAsync($"api/employee/{employeeId}");
            var response = getRequestTask.Result;

            // Assert
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            var employee = response.DeserializeContent<Employee>();
            Assert.AreEqual(expectedFirstName, employee.FirstName);
            Assert.AreEqual(expectedLastName, employee.LastName);
        }

        [TestMethod]
        public void UpdateEmployee_Returns_Ok()
        {
            // Arrange
            var employee = new Employee()
            {
                EmployeeId = "03aa1462-ffa9-4978-901b-7c001562cf6f",
                Department = "Engineering",
                FirstName = "Pete",
                LastName = "Best",
                Position = "Developer VI",
            };
            var requestContent = new JsonSerialization().ToJson(employee);

            // Execute
            var putRequestTask = _httpClient.PutAsync($"api/employee/{employee.EmployeeId}",
               new StringContent(requestContent, Encoding.UTF8, "application/json"));
            var putResponse = putRequestTask.Result;

            // Assert
            Assert.AreEqual(HttpStatusCode.OK, putResponse.StatusCode);
            var newEmployee = putResponse.DeserializeContent<Employee>();

            Assert.AreEqual(employee.FirstName, newEmployee.FirstName);
            Assert.AreEqual(employee.LastName, newEmployee.LastName);
        }

        [TestMethod]
        public void UpdateEmployee_Returns_NotFound()
        {
            // Arrange
            var employee = new Employee()
            {
                EmployeeId = "Invalid_Id",
                Department = "Music",
                FirstName = "Sunny",
                LastName = "Bono",
                Position = "Singer/Song Writer",
            };
            var requestContent = new JsonSerialization().ToJson(employee);

            // Execute
            var postRequestTask = _httpClient.PutAsync($"api/employee/{employee.EmployeeId}",
               new StringContent(requestContent, Encoding.UTF8, "application/json"));
            var response = postRequestTask.Result;

            // Assert
            Assert.AreEqual(HttpStatusCode.NotFound, response.StatusCode);
        }

        [TestMethod]
        public void GetEmployee_DirectReports_Returns_Ok()
        {
            // Make sure the test server is reinitialized with the JSON file as the 
            // other unit tests modify the database.
            _testServer = new TestServer(WebHost.CreateDefaultBuilder()
                .UseStartup<TestServerStartup>()
                .UseEnvironment("Development"));
            _httpClient = _testServer.CreateClient();

            // Create reusable test.
            Action<string, int> testDirectReports = (employeeId, expectedReports) =>
            {
                // Execute
                var getRequestTask = _httpClient.GetAsync($"api/employee/{employeeId}/structure");
                var response = getRequestTask.Result;

                // Assert
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                var reportingStructure = response.DeserializeContent<ReportingStructure>();
                Assert.AreEqual(expectedReports, reportingStructure.NumberOfReports);
                Assert.IsNotNull(reportingStructure.Employee);
            };

            // Test a handful of the employees.
            testDirectReports("16a596ae-edd3-4847-99fe-c4518e82c86f", 4); // John Lennon
            testDirectReports("b7839309-3348-463b-a7e3-5de1c168beb3", 0); // Paul McCartney
            testDirectReports("03aa1462-ffa9-4978-901b-7c001562cf6f", 2); // Ringo Starr
            testDirectReports("62c1084e-6e34-4630-93fd-9153afb65309", 0); // Pete Best
            testDirectReports("c0c2293d-16bd-4603-8e08-638a9d18b22c", 0); // George Harrison
        }

        [TestMethod]
        public void CreateCompensation_Returns_Ok()
        {
            // Arrange
            string employeeId = "16a596ae-edd3-4847-99fe-c4518e82c86f"; // John Lennon
            DateTime expectedEffectiveDate = new DateTime(2019, 9, 6);
            decimal expectedSalary = 46000.50M;

            Compensation compensation = new Compensation()
            {
                EffectiveDate = expectedEffectiveDate,
                Salary = expectedSalary
            };
            var requestContent = new JsonSerialization().ToJson(compensation);

            // Execute
            var postRequestTask = _httpClient.PostAsync($"api/employee/{employeeId}/compensation",
               new StringContent(requestContent, Encoding.UTF8, "application/json"));
            var response = postRequestTask.Result;

            // Assert
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
            var compensationResponse = response.DeserializeContent<Compensation>();
            Assert.AreEqual(expectedEffectiveDate, compensationResponse.EffectiveDate);
            Assert.AreEqual(expectedSalary, compensationResponse.Salary);
            Assert.IsNotNull(compensationResponse.Employee);
            Assert.AreEqual(employeeId, compensationResponse.Employee.EmployeeId);
        }

        [TestMethod]
        public void CreateCompensation_Returns_BadRequest()
        {
            // Arrange
            string employeeId = "16a596ae-edd3-4847-99fe-c4518e82c93f"; // Non-existing employee
            DateTime expectedEffectiveDate = new DateTime(2019, 9, 6);
            decimal expectedSalary = 46000.50M;

            Compensation compensation = new Compensation()
            {
                EffectiveDate = expectedEffectiveDate,
                Salary = expectedSalary
            };
            var requestContent = new JsonSerialization().ToJson(compensation);

            // Execute
            var postRequestTask = _httpClient.PostAsync($"api/employee/{employeeId}/compensation",
               new StringContent(requestContent, Encoding.UTF8, "application/json"));
            var response = postRequestTask.Result;

            // Assert
            Assert.AreEqual(HttpStatusCode.BadRequest, response.StatusCode);
        }

        [TestMethod]
        public void GetCompensation_Returns_Ok()
        {
            // Make sure the test server is reinitialized with the JSON file as the 
            // other unit tests modify the database.
            _testServer = new TestServer(WebHost.CreateDefaultBuilder()
                .UseStartup<TestServerStartup>()
                .UseEnvironment("Development"));
            _httpClient = _testServer.CreateClient();

            // Arrange
            string employeeId = "16a596ae-edd3-4847-99fe-c4518e82c86f";
            decimal salary = 65000.23M;
            DateTime effective = new DateTime(2019, 10, 2);

            // Execute CREATE
            var postRequestTask = _httpClient.PostAsync($"api/employee/{employeeId}/compensation",
               new StringContent(new JsonSerialization().ToJson(new Compensation()
               {
                   Salary = salary,
                   EffectiveDate = effective
               }), Encoding.UTF8, "application/json"));

            // Execute GET
            var getRequestTask = _httpClient.GetAsync($"api/employee/{employeeId}/compensation");
            var response = getRequestTask.Result;

            // Assert
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
            var deserializedResponse = response.DeserializeContent<List<Compensation>>();
            Assert.AreEqual(deserializedResponse.Count, 1);
            Assert.AreEqual(deserializedResponse[0].EffectiveDate, effective);
            Assert.AreEqual(deserializedResponse[0].Salary, salary);
            Assert.IsNotNull(deserializedResponse[0].Employee);
            Assert.AreEqual(employeeId, deserializedResponse[0].Employee.EmployeeId);
        }
    }
}
