﻿using challenge.Models;
using System;
using System.Collections.Generic;

namespace challenge.Services
{
    public interface ICompensationService
    {
        Compensation GetById(String id);
        List<Compensation> GetCompensationByEmployeeId(String employeeId);
        Compensation Create(Compensation compensation);
    }
}