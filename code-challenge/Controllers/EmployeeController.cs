﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using challenge.Services;
using challenge.Models;

namespace challenge.Controllers
{
    [Route("api/employee")]
    public class EmployeeController : Controller
    {
        private readonly ILogger _logger;
        private readonly IEmployeeService _employeeService;
        private readonly ICompensationService _compensationService;

        public EmployeeController(ILogger<EmployeeController> logger, IEmployeeService employeeService, ICompensationService compensationService)
        {
            _logger = logger;
            _employeeService = employeeService;
            _compensationService = compensationService;
        }

        [HttpPost]
        public IActionResult CreateEmployee([FromBody] Employee employee)
        {
            _logger.LogDebug($"Received employee create request for '{employee.FirstName} {employee.LastName}'");

            _employeeService.Create(employee);

            return CreatedAtRoute("getEmployeeById", new { id = employee.EmployeeId }, employee);
        }

        [HttpGet("{id}", Name = "getEmployeeById")]
        public IActionResult GetEmployeeById(String id)
        {
            _logger.LogDebug($"Received employee get request for '{id}'");

            var employee = _employeeService.GetById(id);

            if (employee == null)
                return NotFound();

            return Ok(employee);
        }

        [HttpPut("{id}")]
        public IActionResult ReplaceEmployee(String id, [FromBody]Employee newEmployee)
        {
            _logger.LogDebug($"Recieved employee update request for '{id}'");

            var existingEmployee = _employeeService.GetById(id);
            if (existingEmployee == null)
                return NotFound();

            _employeeService.Replace(existingEmployee, newEmployee);

            return Ok(newEmployee);
        }

        [HttpGet("{id}/structure", Name = "getReportingStructureById")]
        public IActionResult GetReportingStructureById(String id)
        {
            _logger.LogDebug($"Received reporting structure get request for '{id}'");

            Employee employee = _employeeService.GetByIdWithDirectReports(id);

            if (employee == null)
                return NotFound();

            // We have an Employee object, so let's now construct the ReportingStructure.
            ReportingStructure reportingStructure = new ReportingStructure()
            {
                Employee = employee
            };

            return Ok(reportingStructure);
        }

        [HttpGet("{id}/compensation", Name = "getEmployeeCompensationListById")]
        public IActionResult GetEmployeeCompensationListById(String id)
        {
            _logger.LogDebug($"Received compensation get request for '{id}'");

            // Part 2 indicates that new Compensations can be created for an Employee, which means if 
            // we do a GET by EmployeeId then we need to return a list of compensations for that employee.
            List<Compensation> compensationsForEmployee = _compensationService.GetCompensationByEmployeeId(id);

            if (compensationsForEmployee == null)
                return NotFound();

            return Ok(compensationsForEmployee);
        }

        [HttpPost("{id}/compensation")]
        public IActionResult CreateCompensation(String id, [FromBody]Compensation compensation)
        {
            _logger.LogDebug($"Received compensation create request for employee '{id}'");

            // Need to get a clean copy of the Employee object so EF can save the changes without issue.
            Employee employee = _employeeService.GetById(id);

            if (employee == null)
                return BadRequest();

            compensation.Employee = employee;
            _compensationService.Create(compensation);

            return Created("getEmployeeCompensationListById", compensation);
        }
    }
}
