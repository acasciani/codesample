﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using challenge.Models;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using challenge.Data;

namespace challenge.Repositories
{
    public class EmployeeRespository : IEmployeeRepository
    {
        private readonly EmployeeContext _employeeContext;
        private readonly ILogger<IEmployeeRepository> _logger;

        public EmployeeRespository(ILogger<IEmployeeRepository> logger, EmployeeContext employeeContext)
        {
            _employeeContext = employeeContext;
            _logger = logger;
        }

        public Employee Add(Employee employee)
        {
            employee.EmployeeId = Guid.NewGuid().ToString();
            _employeeContext.Employees.Add(employee);
            return employee;
        }

        public Employee GetById(string id)
        {
            return _employeeContext.Employees.SingleOrDefault(e => e.EmployeeId == id);
        }

        /// <summary>
        /// Returns the requested Employee object with its DirectReports loaded in.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Employee GetByIdWithDirectReports(string id)
        {
            // We'll make sure to load the DirectReports for all Employees that are a direct report 
            // to the requested EmployeeId. After we do the Include, we'll call AsEnumerable(). This 
            // will ensure that all Employee objects within all nested DirectReports collections 
            // are loaded up with their DirectReports. AsEnumerable() will maintain deferred execution 
            // so that only the one Employee (specified by EmployeeId == id) is returned, and the entire 
            // collection of Employees is not returned.
            return _employeeContext.Employees
                .Include(i => i.DirectReports)
                .AsEnumerable()
                .SingleOrDefault(i => i.EmployeeId == id);
        }

        public Task SaveAsync()
        {
            return _employeeContext.SaveChangesAsync();
        }

        public Employee Remove(Employee employee)
        {
            return _employeeContext.Remove(employee).Entity;
        }
    }
}
