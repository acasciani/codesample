﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace challenge.Models
{
    /// <summary>
    /// Model for Task 2
    /// </summary>
    public class Compensation
    {
        /// <summary>
        /// Since Compensation is stored in a separate table, it requires a primary key. We'll ignore this during 
        /// serialization/deserialization as it wasn't a requirement of Part 2.
        /// </summary>
        [IgnoreDataMember]
        [Key]
        public String CompensationId { get; set; }

        /// <summary>
        /// The Employee this compensation is for.
        /// </summary>
        public Employee Employee { get; set; }

        /// <summary>
        /// The salary this Employee earns.
        /// </summary>
        public decimal Salary { get; set; }

        /// <summary>
        /// When this compensation becomes effective.
        /// </summary>
        public DateTime EffectiveDate { get; set; }
    }
}
