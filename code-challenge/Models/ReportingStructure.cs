﻿namespace challenge.Models
{
    /// <summary>
    /// Model for Task 1
    /// </summary>
    public class ReportingStructure
    {
        /// <summary>
        /// The employee this reporting structure represents.
        /// </summary>
        public Employee Employee { get; set; }

        /// <summary>
        /// The total number of reports under the given Employee. The number of reports is determined to be 
        /// the number of directReports for an employee and all of their direct reports.
        /// </summary>
        public int NumberOfReports
        {
            get
            {
                // If the Employee or DirectReports is null, then we can't calculate the employee's direct reports.
                if (Employee == null || Employee.DirectReports == null)
                    return 0;

                return CalculateNumberOfDirectReports(Employee);
            }
        }

        /// <summary>
        /// Returns the number of direct reports under the specified Employee.
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        private int CalculateNumberOfDirectReports(Employee employee)
        {
            int totalDirectReports = 0;

            // We will visit each of this Employee's direct reports to see if they have direct reports.
            foreach (Employee directReport in employee.DirectReports)
            {
                if (directReport.DirectReports == null || directReport.DirectReports.Count == 0)
                {
                    // This employee has no direct reports. Just add one for themself.
                    totalDirectReports += 1;
                }
                else
                {
                    // This employee has direct reports. Add one for themself and add their direct reports.
                    totalDirectReports += 1 + CalculateNumberOfDirectReports(directReport);
                }
            }

            return totalDirectReports;
        }
    }
}
